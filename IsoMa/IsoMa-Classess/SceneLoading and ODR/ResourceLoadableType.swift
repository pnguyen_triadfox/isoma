//
//  ResourceLoadableType.swift
//  IsoMa
//
//  Created by Phong Nguyen on 2/27/16.
//  Copyright © 2016 Phong Nguyen. All rights reserved.
//

/// A type capable of loading and managing static resources.
protocol ResourceLoadableType: class {
    /// Indicates that static resources need to be loaded.
    static var resourcesNeedLoading: Bool { get }
    
    /// Loads static resources into memory.
    static func loadResourcesWithCompletionHandler(completionHandler: () -> ())
    
    /// Releases any static resources that can be loaded again later.
    static func purgeResources()
}
