//
//  SceneLoaderResourcesAvailableState.swift
//  IsoMa
//
//  Created by Phong Nguyen on 2/27/16.
//  Copyright © 2016 Phong Nguyen. All rights reserved.
//

import GameplayKit

class SceneLoaderResourcesAvailableState: GKState {
    // MARK: Properties
    
    unowned let sceneLoader: SceneLoader
    
    // MARK: Initialization
    
    init(sceneLoader: SceneLoader) {
        self.sceneLoader = sceneLoader
    }
    
    // MARK: GKState Life Cycle
    
    override func isValidNextState(stateClass: AnyClass) -> Bool {
        switch stateClass {
            case is SceneLoaderInitialState.Type, is SceneLoaderPreparingResourcesState.Type:
                return true
            
            default:
                return false
        }
    }
    
}